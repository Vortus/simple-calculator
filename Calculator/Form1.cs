﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Calculator : Form
    {

        double value = 0; // The value
        String operation = ""; // The operator
        bool operationOnGoing = false;

        public Calculator()
        {
            InitializeComponent();
        }

        private void numberButton_Click(object sender, EventArgs e) // Number handler
        {
            if (operationOnGoing || resultBox.Text == "0") resultBox.Clear();
            operationOnGoing = false;
            Button b = (Button)sender;
            resultBox.AppendText(b.Text);
        }

        private void button16_Click(object sender, EventArgs e) // Clear resultBox
        {
            resultBox.Text = "0";
        }

        private void operator_Click(object sender, EventArgs e) // Operator
        {
            Button b = (Button)sender;
            operation = b.Text;
            value = Double.Parse(resultBox.Text);
            equasionBox.AppendText(value + " " + operation);
            operationOnGoing = true;
        }

        private void button18_Click(object sender, EventArgs e) // = button
        {
            equasionBox.Text = "";
            if (operation == "+")
            {
                resultBox.Text = (value + Double.Parse(resultBox.Text)).ToString();
            }
            else if (operation == "-")
            {
                resultBox.Text = (value - Double.Parse(resultBox.Text)).ToString();
            }
            else if (operation == "*")
            {
                resultBox.Text = (value * Double.Parse(resultBox.Text)).ToString();
            }
            else if (operation == "/")
            {
                resultBox.Text = (value / Double.Parse(resultBox.Text)).ToString();
            }
        }

        private void button17_Click(object sender, EventArgs e) // C
        {
            resultBox.Clear();
            value = 0;
        }
    }
}
